export default function mainColumns() {

const columns = [
    { 
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Gender',
      dataIndex: 'gender',
      key: 'gender',
    },
    {
      title: 'Birth Year',
      dataIndex: 'birth_year',
      key: 'birth_year',
    },
  ];

  return columns;

}