import React, {useEffect, useState} from 'react';
import { 
  Table ,
  PageHeader,
  Descriptions,
} from 'antd';
import "antd/dist/antd.css";
import './App.css';
import mainColumns from './column';

function useFetch(url){
  const [data, refetchData] = useState();
  const [loading, setLoading] = useState(true);

  console.log(data);

  useEffect(()=>{
    setLoading(true);
    fetch(url)
      .then(item => item.json())
      .then(data => {
        refetchData(data)
        setLoading(false)
      })
  },[url]);

  function refetch(newAPI) {
    refetchData(newAPI);
  }

  return [{data, loading}, refetch]
}

function reformData(data){
  const listData = data.map(item => ({
    key         : item.name,
    birth_year  : item.birth_year,
    created     : item.created,  
    edited      : item.edited,
    eye_color   : item.eye_color,
    gender      : item.gender,
    hair_color  : item.hair_color,
    height      : item.height,
    homeworld   : item.homeworld,
    mass        : item.mass,
    name        : item.name,
    skin_color  : item.skin_color,
    species     : item.species,
    starships   : item.starships,
    url         : item.url,
    vehicles    : item.vehicles,
  }));
  return listData;
}


function App() {

  const [{data, loading}] = useFetch("https://swapi.dev/api/people/");
  const [dataPeople, setDataPeople] = useState();
  
  const [isExpanded, setIsExpanded] = useState(false);
  const [homeworldAPI, setHomeWorldAPI] = useState();


  const columns = mainColumns();

  console.log(data);
  console.log(dataPeople);

  useEffect(()=>{
    if(data != undefined){
      setDataPeople(data.results);
    }
  },[loading]);

  useEffect(()=>{
    console.log(homeworldAPI);
  },[isExpanded]);

  const onExpand = (expanded, record) => {
    setIsExpanded(expanded);
    if(expanded){
      setHomeWorldAPI(record.homeworld);
    }
  }

  return (
    <>
      <div>
        <PageHeader
          className="site-page-header"
          title="SWAPI"
          subTitle="The Story: Star Wars Database!"
        />
      </div>
      <div>
        <Table 
        columns={columns}
        expandable={{ 
         expandedRowRender : record => (
            <Descriptions title={record.name + " info"} bordered> 
              <Descriptions.Item label={"Gender"}>{record.gender}</Descriptions.Item>
              <Descriptions.Item label={"Birth Year"}>{record.birth_year}</Descriptions.Item>
              <Descriptions.Item label={"Eye Color"}>{record.eye_color}</Descriptions.Item>
              <Descriptions.Item label={"Hair Color"}>{record.hair_color}</Descriptions.Item>
              <Descriptions.Item label={"Height"}>{record.height}</Descriptions.Item>
              <Descriptions.Item label={"Mass"}>{record.mass}</Descriptions.Item>
              <Descriptions.Item label={"SKin Color"}>{record.skin_color}</Descriptions.Item>
            </Descriptions>
          )}}
        dataSource={dataPeople !== undefined ? reformData(dataPeople) : []} 
        pagination={false}
        onExpand={onExpand}
        /> 
      </div>
    </>
  );
}

export default App;
